'use strict';

// necessary because in MySQL DDL (create, alter, drop) are autocommited and transition rollback doesn't work
async function rollback(queryInterface) {
  await queryInterface.sequelize.query('DROP TABLE IF EXISTS users');
}

module.exports = {
  async up (queryInterface) {
    try {
      await queryInterface.sequelize.query(`
        CREATE TABLE users (
          id INT AUTO_INCREMENT,
          user VARCHAR(255) NOT NULL,
          password VARCHAR(255) NOT NULL,
          token VARCHAR(500) NOT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
          deleted_at DATETIME,
          PRIMARY KEY (id)
        )
      `);

      // await queryInterface.sequelize.query(`
      //   ALTER TABLE users
      //     ADD ...
      // `);
    } catch (error) {
      await rollback(queryInterface);
      console.error('\x1b[31m%s\x1b[0m', 'Migration create-users (up):');
      throw error;
    }
  },

  async down (queryInterface) {
    try {
      await rollback(queryInterface);
    } catch (error) {
      console.error('\x1b[31m%s\x1b[0m', 'Migration create-users (down):');
      throw error;
    }
  }
};
