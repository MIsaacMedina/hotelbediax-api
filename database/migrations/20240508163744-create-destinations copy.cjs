'use strict';

// necessary because in MySQL DDL (create, alter, drop) are autocommited and transition rollback doesn't work
async function rollback(queryInterface) {
  await queryInterface.sequelize.query('DROP TABLE IF EXISTS destinations');
}

module.exports = {
  async up (queryInterface) {
    try {
      await queryInterface.sequelize.query(`
        CREATE TABLE destinations (
          id INT AUTO_INCREMENT,
          name VARCHAR(255) NOT NULL,
          description VARCHAR(5000) NOT NULL,
          country_code VARCHAR(3) NOT NULL,
          type ENUM('hotel', 'apartment', 'aparthotel', 'hostel', 'pension', 'camping', 'house') NOT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
          deleted_at DATETIME,
          PRIMARY KEY (id)
        )
      `);

      // await queryInterface.sequelize.query(`
      //   ALTER TABLE destinations
      //     ADD ...
      // `);
    } catch (error) {
      await rollback(queryInterface);
      console.error('\x1b[31m%s\x1b[0m', 'Migration create-destinations (up):');
      throw error;
    }
  },

  async down (queryInterface) {
    try {
      await rollback(queryInterface);
    } catch (error) {
      console.error('\x1b[31m%s\x1b[0m', 'Migration create-destinations (down):');
      throw error;
    }
  }
};
