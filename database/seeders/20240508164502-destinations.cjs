module.exports = {
  up: async (queryInterface) => {
    let transaction;
    try {
      transaction = await queryInterface.sequelize.transaction();

      await queryInterface.bulkInsert('destinations', [
        {
          name: 'Hotel Riu Palace Palmeras',
          description: 'Hotel Riu Palace Palmeras has an outdoor swimming pool, garden, a fitness centre and bar in San Bartolomé. Boasting a 24-hour front desk, this property also welcomes guests with a restaurant and a terrace. Each room includes a balcony.',
          country_code: 'ESP',
          type: 'hotel',
        },
        {
          name: 'INNER Kompas Palmanova "Adults Only"',
          description: 'Situated in Palmanova, just 100 metres from Palma Nova Beach, INNER Kompas Palmanova "Adults Only" features beachfront accommodation with a bar and free WiFi. It is set 500 metres from Son Maties Beach and provides full-day security. The property offers allergy-free units and is located 500 metres from Es Carregador Beach.',
          country_code: 'ESP',
          type: 'apartment',
        },
        {
          name: 'Marissa Resort',
          description: 'Marissa Resort offers a sauna and free private parking, and is within 42 km of Osnabrueck Central Station and 42 km of University of Osnabrueck. Set on the beachfront, this property has a spa and wellness centre, an open-air bath and a garden. The apartment features an indoor pool with a water slide, as well as a hot tub and a lift.',
          country_code: 'DEU',
          type: 'apartment',
        },
        {
          name: 'Beautiful Character Home, Parking Garden WiFi Self Check-in',
          description: 'Featuring inner courtyard views, Beautiful Character Home, Parking Garden WiFi Self Check-in provides accommodation with a garden and a patio, around 15 km from Woburn Abbey. Both free WiFi and parking on-site are available at the holiday home free of charge. The property is non-smoking and is located 28 km from Bletchley Park.',
          country_code: 'GBR',
          type: 'house',
        },
        {
          name: 'Hotel Zehnthof',
          description: 'Located beside the River Moselle, this family-run hotel in the wine town of Cochem offers spacious rooms, varied buffet breakfasts, and free parking. Cochem Train Station is 500 metres away. Featuring a wooden- framed façade, the Hotel Zehnthof has brightly decorated rooms with a desk, and private bathroom.There is a self- service bar and free Wi - Fi is available in public areas.',
          country_code: 'DEU',
          type: 'hotel',
        },
        {
          name: 'Albertus Paris Aparthotel',
          description: 'These non-smoking suites and studio apartments are in Hamburg’s north-eastern district of Wandsbek. Quick public transport connects to the city centre, the Reeperbahn entertainment district, harbour, and fish market. The brightly decorated Albertus Paris Stadt- Appartements are in a quiet side street.They are 10 minutes from Hamburg city centre by S - Bahn(city rail) and underground train.',
          country_code: 'DEU',
          type: 'aparthotel',
        }
      ], { transaction });

      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      console.error('\x1b[31m%s\x1b[0m', 'Seeder destinations (up):');
      throw error;
    }
  },

  down: async (queryInterface) => {
    try {
      await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
      await queryInterface.sequelize.query('TRUNCATE TABLE destinations');
      await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
    } catch (error) {
      console.error('\x1b[31m%s\x1b[0m', 'Seeder destinations (down):');
      throw error;
    }
  }
};
