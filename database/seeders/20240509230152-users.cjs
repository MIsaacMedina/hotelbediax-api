module.exports = {
  up: async (queryInterface) => {
    let transaction;
    try {
      transaction = await queryInterface.sequelize.transaction();

      await queryInterface.bulkInsert('users', [
        {
          user: 'Admin',
          password: 'Admin-password',
          token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlciI6IkFkbWluIiwicGFzc3dvcmQiOiJBZG1pbi1wYXNzd29yZCIsImlhdCI6MTcxNTI5ODg1NiwiZXhwIjoxNzE2MjcwODU2fQ.lvR5ScKyEFVYfgFQseRHo3_5T59OAcIhv3fJ4ln6DMs',
        },
        {
          user: 'Test-user',
          password: 'Test-password',
          token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlciI6IlRlc3QtdXNlciIsInBhc3N3b3JkIjoiVGVzdC1wYXNzd29yZCIsImlhdCI6MTcxNTI5ODkyMiwiZXhwIjoxNzE2MjcwOTIyfQ.qwFumCjWcrlyhzqqPFT7tlZlzw32qvfQQ_-NgaGOEK4',
        }
      ], { transaction });

      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      console.error('\x1b[31m%s\x1b[0m', 'Seeder users (up):');
      throw error;
    }
  },

  down: async (queryInterface) => {
    try {
      await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0');
      await queryInterface.sequelize.query('TRUNCATE TABLE users');
      await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1');
    } catch (error) {
      console.error('\x1b[31m%s\x1b[0m', 'Seeder users (down):');
      throw error;
    }
  }
};
