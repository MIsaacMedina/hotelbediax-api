# Use a Node.js images as base
FROM node:20.13.0

# Container work directory on /app
WORKDIR /app

# Copy package.json and package-lock.json to install dependencies first
COPY package*.json ./

# Install dependencies
RUN npm i

# Copy all app files
COPY . .

# Exposing por 3000 in container
EXPOSE 3000

# Run migrations, seeders and start server
CMD npm run migrate:dev && npm run seed:dev && npm start
