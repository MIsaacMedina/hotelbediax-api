import nReadlines from 'n-readlines';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let envPath = path.join(__dirname, '../../.env');
if (process.env.NODE_ENV === 'test') {
  envPath = path.join(__dirname, '../../.env.test');
}
const broadbandLines = new nReadlines(envPath);

let env = { NODE_ENV: process.env.NODE_ENV };
let line = broadbandLines.next();
// Read every single environment variable from .env and save it in "env"
while (line) {
  const lineText = line.toString('ascii');
  if (lineText.startsWith('#')) {
    line = broadbandLines.next();
    continue;
  }

  const key = lineText.split('=')[0].trim();
  const value = lineText.split('=')[1].trim();
  
  if (value === 'true') env[key] = true;
  else if (value === 'false') env[key] = false;
  else if (!isNaN(Number(value))) env[key] = parseInt(value);
  else env[key] = value;

  line = broadbandLines.next();
}

export default env;
