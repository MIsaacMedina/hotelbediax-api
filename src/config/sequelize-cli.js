import env from '#config/env.js';

export default {
  // process.env.NODE_ENV === "development"
  development: {
    username: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME,
    host: env.DB_HOST,
    port: env.DB_PORT,
    dialect: 'mysql',
    migrationStorage: 'sequelize', // save in it's database table
    migrationStorageTableName: 'sequelize_migrations',
    seederStorage: 'sequelize', // save in it's database table
    seederStorageTableName: 'sequelize_seeders',
    // dialectOptions: {
    //   bigNumberStrings: true
    // }
  },
  // process.env.NODE_ENV === "production"
  production: {
    username: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME,
    host: env.DB_HOST,
    port: env.DB_PORT,
    dialect: 'mysql',
    migrationStorage: 'sequelize', // save in it's database table
    migrationStorageTableName: 'sequelize_migrations',
    seederStorage: 'sequelize', // save in it's database table
    seederStorageTableName: 'sequelize_seeders',
    // dialectOptions: {
    //   bigNumberStrings: true,
    //   ssl: {
    //     ca: fs.readFileSync(__dirname + '/mysql-ca-main.crt')
    //   }
    // }
  }
};