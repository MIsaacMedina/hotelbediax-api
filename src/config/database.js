import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import Sequelize from 'sequelize';
import env from '#config/env.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Instantiate Sequelize
const sequelize = new Sequelize (
  env.DB_NAME,
  env.DB_USER,
  env.DB_PASSWORD,
  {
    host: env.DB_HOST,
    port: env.DB_PORT,
    dialect: 'mysql',
    logging: (sql) => env.DEBUG && console.log('\x1b[1m%s\x1b[0m',sql),
    define: {
      timestamps: true,
      paranoid: true, // update deleted_at when destroy
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    },
    pool: {
      max: 10,
      min: 0,
      acquire: 20000,
      idle: 10000,
    },
  }
);

function getModelPaths() {
  const components = path.join(__dirname, '..', 'components');
  // files & folders sons of components folder
  const elementsInComponents = fs.readdirSync(components);
  const dirs = elementsInComponents.filter(dir => {
    if (dir.startsWith('0 ')) return false;
    let stat = fs.statSync(path.join(components, dir));
    return stat.isDirectory();
  });
  const modelPaths = dirs.map(dir => {
    let stat = fs.statSync(path.join(components, dir));
    const isDirectory = stat.isDirectory();
    if (isDirectory) {
      const componentFiles = fs.readdirSync(path.join(components, dir));
      if (componentFiles.includes('model.js')) {
        return path.join(components, dir, 'model.js');
      }
    }
  });
  return modelPaths.filter(path => path != undefined);
}

async function instantiateModels(modelPaths) {
  const models = {};
  await Promise.all(modelPaths.map(async (modelPath) => {
    const { default: model } = await import(modelPath);
    models[model.name] = model.init(sequelize, Sequelize);
  }));
  return models;
}

const modelPaths = getModelPaths();
// Init all models first to avoid association errors
const models = await instantiateModels(modelPaths);

const db = {
  ...models,
  sequelize
};

// Create associations (ORM relationships)
Object.values(models)
  .filter(model => model.associate && typeof model.associate === 'function')
  .forEach(model => model.associate(models));

// Database connection test
(async () => {
  try {
    await sequelize.authenticate();
    console.info('\x1b[36m%s\x1b[0m', 'Sequelize connected to db');
  } catch (error) {
    console.error('\x1b[31m%s\x1b[0m', `Unable to connect to the database -> ${error}`);
  }
})();

export default db;
