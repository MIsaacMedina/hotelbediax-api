import express from 'express';
import path from 'path';
import cors from 'cors';
import morgan from 'morgan';
import routes from '#routes/index.js';
import errorHandler from '#utils/errorHandler.js';

const app = express();

app.use(cors());
// parse req.body to json
app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev')); // To show requests in terminal

// remove any server information
app.disable('x-powered-by');

// routes
app.use('/v1/', routes);

// public directory
// app.use('/statics', express.static(path.join(__dirname,'../public')));

// errors
app.use((request, response) => errorHandler.error404(request, response));

export default app;
