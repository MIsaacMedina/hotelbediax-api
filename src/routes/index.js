import { Router } from 'express';
import env from '#config/env.js';
import destinations from '#destinations/routes.js';
import users from '#users/routes.js';
import jwt from '#utils/jwt.js';

// Endpoints

const router = Router();
router.get('/', async (request, response) => {
  response.json({
    name: 'TDMP API',
    description: 'Tourist destination management portal API',
    author: 'Isaac Medina Nuez',
    environment: env.NODE_ENV,
    host: env.HOST,
    port: env.PORT
  });
});

// Endpoint to get a token for a given object { user, password, hours }
// router.post('/generate-jwt', async (request, response) => {
//   const { user, password, hours = 270 } = request.body;
//   if (!user || !password) return response.status(404).send(`'user' and 'password' params are mandatory. 'hours' optional`);

//   const token = jwt.create({ id: 2, user, password }, hours);
//   response.json({ token });
// });

router.use('/destinations', destinations);
router.use('/users', users);

export default router;
