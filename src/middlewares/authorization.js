import jwt from '#utils/jwt.js';
import db from '#config/database.js';
import errorHandler from '#utils/errorHandler.js';

export const verifyToken = async (request, response, next) => {
  try {
    const token = request.headers['authorization'];
    
    if (!token || !token.startsWith('Bearer ')) {
      return errorHandler.error401(request, response, 'Token not provided');
    }
    
    // Decode token (without 'Bearer ')
    const tokenDecoded = jwt.verify(token.substring(7));
    
    const user = await db.User.findByPk(tokenDecoded.id, { raw: true });
    if (!user) return errorHandler.error404(request, response, 'User not found');
    
    // Set user ID in header param JWTUID
    request.params.JWTUID = tokenDecoded.id;
    
    next();
  } catch (error) {
    //console.log(error);
    return errorHandler.error403(request, response);
  }
};
