import express from '#config/express.js';
import env from '#config/env.js';

console.info('\x1b[36m%s\x1b[0m', `Environment: ${env.NODE_ENV}`);

try {
  // HTTP
  express.listen(env.PORT, env.HOST, () => {
    console.info('\x1b[36m%s\x1b[0m', `Server running on http://${env.HOST}:${env.PORT}`);
  });
} catch (error) {
  console.error('\x1b[31m%s\x1b[0m', error);
}
