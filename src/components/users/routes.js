import { Router } from 'express';
import { verifyToken } from '#middlewares/authorization.js';
import UsersController from './controller.js';

const router = Router();

// Refresh token
router.get('/refresh/:id?', [verifyToken], UsersController.refreshToken);

// CRUD
router.post('/', [verifyToken], UsersController.create);
router.get('/:id?', [verifyToken], UsersController.read);
router.put('/:id', [verifyToken], UsersController.update);
router.delete('/:id', [verifyToken], UsersController.delete);

// Partial update
router.patch('/:id', [verifyToken], UsersController.partialUpdate);

// Recover soft deleted record
router.delete('/recover/:id', [verifyToken], UsersController.recover);


export default router;
