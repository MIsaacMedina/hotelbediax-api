import { Model } from 'sequelize';

export default class User extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER(11),
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        user: {
          type: Sequelize.STRING(255),
          allowNull: false,
        },
        password: {
          type: Sequelize.STRING(255),
          allowNull: false,
        },
        token: {
          type: Sequelize.STRING(500),
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'users',
        tableName: 'users',
        timestamps: true,
        paranoid: true, // update deleted_at when destroy
      }
    );
  }

  // Database relationships
  static associate(models) {
    // this.belongsToMany(models.User, {
    //   as: 'users',
    //   through: 'companies_users',
    //   foreignKey: 'company_id'
    // });
  }
}
