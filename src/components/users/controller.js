import db from '#src/config/database.js';
import errorHandler from '#utils/errorHandler.js';
import cleanObject from '#utils/cleanObject.js';
import usersAdapter from './adapter.js';
import jwt from '#utils/jwt.js';

const tableName = 'users';
const model = db.User;
const defaultExpiration = 24 * 30; // 1 month

// CRUD + Patch

export default {
  create: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const token = jwt.create({ id: 2, user: request.body.user, password: request.body.password }, defaultExpiration);
      const user = {
        user: request.body.user,
        password: request.body.password,
        token,
      };
      
      const row = await usersAdapter.create({ object: user, tableName, model });
      return response.status(201).json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error400(request, response, error);
      else return errorHandler.handler(request, response, error);
    }
  },
  read: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      const id = request.params.id;

      let row;
      if (id) row = await usersAdapter.find({ id, tableName, model });
      else {
        const filter = request.query.filter && JSON.parse(request.query.filter) || null;
        const order = request.query.order && JSON.parse(request.query.order) || null;
        const itemsPerPage = request.query.items_per_page || 10;
        const page = request.query.page || 1;

        row = await usersAdapter.list({ tableName, model, filter, order, itemsPerPage, page });
      }

      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  update: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      const user = {
        id: request.params.id,
        user: request.body.user || null,
        password: request.body.password || null,
        token: request.body.token || null,
      };
      
      const rowUpdated = await usersAdapter.update({ object: user, tableName, model });
      if (!rowUpdated) return errorHandler.error400(request, response, `Error updating record with ID ${user.id} in "${tableName}"`);
      
      return response.json({ data: rowUpdated });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  partialUpdate: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      let user = {
        id: request.params.id,
        user: request.body.user || null,
        password: request.body.password || null,
        token: request.body.token || null,
      };
      
      user = cleanObject(user);

      const rowUpdated = await usersAdapter.update({ object: user, tableName, model });
      if (!rowUpdated) return errorHandler.error400(request, response, `Error updating record with ID ${user.id} in "${tableName}"`);
      
      return response.json({ data: rowUpdated });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  delete: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const id = request.params.id;
      const hardDelete = request.params.hard_delete;
      
      const row = await usersAdapter.delete({ id, tableName, model, hardDelete });
      
      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  recover: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const id = request.params.id;
      const row = await usersAdapter.recover({ id, tableName, model });
      if (!row) return errorHandler.error400(request, response, `Error recovering "${tableName}"`);

      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  refreshToken: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const id = request.params.id || JWTUID; // Update user passed by param or user logged
      const token = jwt.create({ id: 2, user: request.body.user, password: request.body.password }, defaultExpiration);

      const user = {
        id,
        token,
      };

      const rowUpdated = await usersAdapter.update({ object: user, tableName, model });
      if (!rowUpdated) return errorHandler.error400(request, response, `Error updating record with ID ${user.id} in "${tableName}"`);

      return response.json({ token: rowUpdated.token });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
};
