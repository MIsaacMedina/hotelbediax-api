// TODO: replate all "Templates" with your database table name camelized & capitalizes
import { Router } from 'express';
import { verifyToken } from '#middlewares/authorization.js';
import TemplatesController from './controller.js';

const router = Router();

// CRUD
router.post('/', [verifyToken], TemplatesController.create);
router.get('/:id?', [verifyToken], TemplatesController.read);
router.put('/:id', [verifyToken], TemplatesController.update);
router.delete('/:id', [verifyToken], TemplatesController.delete);

// Partial update
router.patch('/:id', [verifyToken], TemplatesController.partialUpdate);

// Recover soft deleted record
router.delete('/recover/:id', [verifyToken], TemplatesController.recover);

export default router;
