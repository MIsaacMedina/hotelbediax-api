import { Model } from 'sequelize';

// TODO: replace the class name with your database table name camelized & capitalizes
export default class Template extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER(11),
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING(255),
          allowNull: false,
        },
        description: {
          type: Sequelize.STRING(5000),
          allowNull: false,
        },
        country_code: {
          type: Sequelize.STRING(3),
          allowNull: false,
        },
        type: {
          type: Sequelize.ENUM(['hotel', 'apartment', 'aparthotel', 'hostel', 'pension', 'camping', 'house']),
          allowNull: true,
        },
      },
      {
        sequelize,
        modelName: 'templates', // TODO: replace with your database table name
        tableName: 'templates', // TODO: replace with your database table name
      }
    );
  }

  // Database relationships
  static associate(models) {
    // this.belongsToMany(models.User, {
    //   as: 'users',
    //   through: 'companies_users',
    //   foreignKey: 'company_id'
    // });
  }
}
