// TODO: replate all "templates" with your model in plural lower cased
// TODO: replate all "template" with your model in singular lower cased
import db from '#src/config/database.js';
import errorHandler from '#utils/errorHandler.js';
import cleanObject from '#utils/cleanObject.js';
import templatesAdapter from './adapter.js';

const tableName = 'templates';
const model = db.Template; // TODO: replace by your model class name

// CRUD + Patch

export default {
  create: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const template = {
        name: request.body.name,
        description: request.body.description,
        country_code: request.body.country_code,
        type: request.body.type,
      };
      
      const row = await templatesAdapter.create({ object: template, tableName, model });
      return response.status(201).json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error400(request, response, error);
      else return errorHandler.handler(request, response, error);
    }
  },
  read: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      const id = request.params.id;

      let row;
      if (id) row = await templatesAdapter.find({ id, tableName, model });
      else {
        const filter = request.query.filter && JSON.parse(request.query.filter) || null;
        const order = request.query.order && JSON.parse(request.query.order) || null;
        
        row = await templatesAdapter.list({ tableName, model, filter, order });
      }

      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  update: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      const template = {
        id: request.params.id,
        name: request.body.name || null,
        description: request.body.description || null,
        country_code: request.body.country_code || null,
        type: request.body.type || null,
      };
      
      const rowUpdated = await templatesAdapter.update({ object: template, tableName, model });
      if (!rowUpdated) return errorHandler.error400(request, response, `Error updating record with ID ${template.id} in "${tableName}"`);
      
      return response.json({ data: rowUpdated });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  partialUpdate: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      let template = {
        id: request.params.id || null,
        name: request.body.name || null,
        description: request.body.description || null,
        country_code: request.body.country_code || null,
        type: request.body.type || null,
      };
      
      template = cleanObject(template);

      const rowUpdated = await templatesAdapter.update({ object: template, tableName, model });
      if (!rowUpdated) return errorHandler.error400(request, response, `Error updating record with ID ${template.id} in "${tableName}"`);
      
      return response.json({ data: rowUpdated });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  delete: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const id = request.params.id;
      const hardDelete = request.params.hard_delete;
      
      const row = await templatesAdapter.delete({ id, tableName, model, hardDelete });
      
      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  recover: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const id = request.params.id;
      const row = await templatesAdapter.recover({ id, tableName, model });
      if (!row) return errorHandler.error400(request, response, `Error recovering "${tableName}"`);

      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
};
