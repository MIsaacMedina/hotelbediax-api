import { Router } from 'express';
import { verifyToken } from '#middlewares/authorization.js';
import DestinationsController from './controller.js';

const router = Router();

// CRUD
router.post('/', [verifyToken], DestinationsController.create);
router.get('/:id?', [verifyToken], DestinationsController.read);
router.put('/:id', [verifyToken], DestinationsController.update);
router.delete('/:id', [verifyToken], DestinationsController.delete);

// Partial update
router.patch('/:id', [verifyToken], DestinationsController.partialUpdate);

// Recover soft deleted record
router.delete('/recover/:id', [verifyToken], DestinationsController.recover);

export default router;
