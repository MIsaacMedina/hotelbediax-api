import commonAdapter from "#utils/commonAdapter.js";

/**
 * Get the WHERE clause without the "WHERE" keyword
 * @param {string} field DDBB table field
 * @param {string} value Value to filter by
 * @returns {string} SQL WHERE clause
 */
function getFilterSql(field, value) {
  switch (field) {
    case 'id':
      return `\`${field}\` in (${value})`;
    case 'name':
    case 'description':
      return `\`${field}\` like '%${value}%'`;
    case 'country_code':
    case 'type':
      return `\`${field}\` = '${value}'`;
    default:
      return '';
  }
}

export default {
  ...commonAdapter,
  /**
   * Get a list of data optionally filtered
   * @param {Object} params
   * @param {string} params.tableName Table name as we created in DDBB
   * @param {sequelizeModel} params.model Sequelize model instance
   * @param {Object[]} params.filter Data to filter [{ field: 'name', value: 'riu' }, { field: 'type', value: 'hotel' }]
   * @param {Object[]} params.order Field and ordenation type to order [{ field: 'id', type: 'ASC' }, { field: 'name', type: 'DESC' }]
   * @param {number} params.itemsPerPage Number of records to retrieve
   * @param {number} params.page Page to retrieve (from 1)
   * @returns {expressResponse|raw} 
   */
  async list({ tableName, model, filter, order, itemsPerPage = 10, page = 1 }) {
    try {
      let where = 'WHERE deleted_at IS NULL AND ';
      if (filter?.length) {
        filter.forEach(element => {
          const filterSql = getFilterSql(element.field, element.value);
          if (filterSql.length) where = `${where} ${filterSql} AND `
        });
      }
      where = where.slice(0, -5); // remove last ' AND '

      let orderBy = '';
      if (order?.length) {
        orderBy = 'ORDER BY';
        order.forEach(element => {
          orderBy = `${orderBy} \`${element.field}\` ${element.type},`
        });
        orderBy = orderBy.slice(0, -1); // remove last comma
      }

      const { rows, total } = await commonAdapter.listFiltered({ tableName, model, where, orderBy, itemsPerPage, page });

      return { rows, total, itemsPerPage, page };
    } catch (error) {
      throw error;
    }
  },
};