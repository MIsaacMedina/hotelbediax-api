import db from '#src/config/database.js';
import errorHandler from '#utils/errorHandler.js';
import cleanObject from '#utils/cleanObject.js';
import destinationsAdapter from './adapter.js';

const tableName = 'destinations';
const model = db.Destination;

// CRUD + Patch

export default {
  create: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const destination = {
        name: request.body.name,
        description: request.body.description,
        country_code: request.body.country_code,
        type: request.body.type,
      };
      
      const row = await destinationsAdapter.create({ object: destination, tableName, model });
      return response.status(201).json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error400(request, response, error);
      else return errorHandler.handler(request, response, error);
    }
  },
  read: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      const id = request.params.id;

      let rows;
      if (id) rows = await destinationsAdapter.find({ id, tableName, model });
      else {
        const filter = request.query.filter && JSON.parse(request.query.filter) || null;
        const order = request.query.order && JSON.parse(request.query.order) || null;
        const itemsPerPage = request.query.items_per_page || 10;
        const page = request.query.page || 1;
        
        rows = await destinationsAdapter.list({ tableName, model, filter, order, itemsPerPage, page });
      }

      return response.json({ data: rows });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  update: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      const destination = {
        id: request.params.id,
        name: request.body.name || null,
        description: request.body.description || null,
        country_code: request.body.country_code || null,
        type: request.body.type || null,
      };
      
      const rowUpdated = await destinationsAdapter.update({ object: destination, tableName, model });
      if (!rowUpdated) return errorHandler.error400(request, response, `Error updating record with ID ${destination.id} in "${tableName}"`);
      
      return response.json({ data: rowUpdated });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  partialUpdate: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);

      let destination = {
        id: request.params.id || null,
        name: request.body.name || null,
        description: request.body.description || null,
        country_code: request.body.country_code || null,
        type: request.body.type || null,
      };
      
      destination = cleanObject(destination);

      const rowUpdated = await destinationsAdapter.update({ object: destination, tableName, model });
      if (!rowUpdated) return errorHandler.error400(request, response, `Error updating record with ID ${destination.id} in "${tableName}"`);
      
      return response.json({ data: rowUpdated });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  delete: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const id = request.params.id;
      const hardDelete = request.query.hard_delete;

      const row = await destinationsAdapter.delete({ id, tableName, model, hardDelete });
      
      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
  recover: async (request, response) => {
    try {
      const JWTUID = request.params.JWTUID;
      if (!JWTUID) return errorHandler.error401(request, response);
      
      const id = request.params.id;
      const row = await destinationsAdapter.recover({ id, tableName, model });
      if (!row) return errorHandler.error400(request, response, `Error recovering "${tableName}"`);

      return response.json({ data: row });
    } catch (error) {
      if (typeof error === 'string') return errorHandler.error404(request, response, error);
      return errorHandler.handler(request, response, error);
    }
  },
};
