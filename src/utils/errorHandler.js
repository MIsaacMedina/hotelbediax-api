import { ValidationError } from 'sequelize';
import env from '#config/env.js';

const errorHandler = {
  error: (request, response, code, text) => {
    return response.status(code).json({ errors: [text ? text : 'Request failed'] });
  },
  error400: (request, response, text) => {
    return response.status(400).json({ errors: [text ? text : 'Request failed'] });
  },
  error401: (request, response, text) => {
    return response.status(401).json({ errors: [text ? text : 'Unauthorized'] });
  },
  error403: (request, response, text) => {
    return response.status(403).json({ errors: [text ? text : 'You do not have permissions'] });
  },
  error404: (request, response, text) => {
    return response.status(404).json({ errors: [text ? text : `Not found route [${request.method}] '${request.path}'`] });
  },
  error422: (request, response, text) => {
    return response.status(422).json({ errors: [text ? text : 'Review the form errors'] });
  },
  error500: (request, response, error, text) => {
    sendErrorToLog(request, error);
    return response.status(500).json({ errors: [text ? text : 'Ups, something happened'] });
  },
  handler: (request, response, error) => {
    if (env.DEBUG) console.error('\x1b[31m%s\x1b[0m', 'ERROR: ', error);
    if (error instanceof ValidationError) {
      return response.status(422).json({ errors: error.errors.map(item => item.message) });
    }
    if (error.name && error.name === 'SequelizeDatabaseError') {
      return response.status(422).json({ errors: [error.original.sqlMessage] });
    }

    sendErrorToLog(request, error);
    return response.status(500).json({ errors: ['Ups, something happened'] });
  }
};

async function sendErrorToLog(request, error) {
  // Logic if you want to send the error to a external logger when anything fails
}

export default errorHandler;