/**
 * Remove null and undefined from an object recursively
 * @param {Object} object Object to clean
 * @returns {Object} object without fields with null or undefined values
 */
function cleanObject(object) {
  return Object.entries(object)
    .map(([key, value]) => [key, value && typeof value === "object" ? cleanEmpty(value) : value])
    .reduce((result, [key, value]) => (value == null ? result : (result[key] = value, result)), {});
}

export default cleanObject;
