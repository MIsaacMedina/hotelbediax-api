import db from '#config/database.js';

export default {
  /**
   * Create a new entry in DDBB
   * @param {Object} params
   * @param {Object} params.object Object to create
   * @param {string} params.tableName Table name as we created in DDBB
   * @param {sequelizeModel} params.model Sequelize model instance
   * @returns {Object} Record created
   */
  async create({ object, tableName, model }) {
    try {
      const row = await model.create(object);
      if (!row) throw `Error creating an entry in '${tableName}'`;

      return row;
    } catch (error) {
      throw error;
    }
  },
  /**
   * Get a row by its primary key ("id" in request params)
   * @param {object} params
   * @param {number} params.id Record unique ID
   * @param {string} params.tableName Table name as we created in DDBB
   * @param {sequelizeModel} params.model Sequelize model instance
   * @param {boolean} params.intercept If return raw response or as an standard object { data: rawResponse }
   * @returns {Object} Record found
   */
  async find({ id, tableName, model }) {
    try {
      const row = await model.findByPk(id);
      if (!row) throw `ID ${id} in '${tableName}' not found`;

      return row;
    } catch (error) {
      throw error;
    }
  },
  /**
   * Get a list of data optionally filtered
   * @param {Object} params
   * @param {string} params.tableName Table name as we created in DDBB
   * @param {sequelizeModel} params.model Sequelize model instance
   * @param {Object[]} params.where WHERE SQL clause
   * @param {Object[]} params.orderBY ORDERBY SQL clause
   * @returns {expressResponse|raw} 
   */
  async listFiltered({ tableName, model, where = '', orderBy = '', itemsPerPage = 10, page = 1 }) {
    try {
      const initialRow = (page - 1) * itemsPerPage
      const limit = `LIMIT ${initialRow},${itemsPerPage}`;

      const sql = `SELECT * FROM ${tableName} ${where} ${orderBy} ${limit}`.trim();
      const sqlTotal = `SELECT count(*) total FROM ${tableName} ${where} ${orderBy}`.trim();

      const rows = await db.sequelize.query(sql, { model });
      const total = await db.sequelize.query(sqlTotal, { plain: true });

      return { rows, total: total.total, itemsPerPage, page };
    } catch (error) {
      throw error;
    }
  },
  /**
   * Update an entry in DDBB
   * @param {Object} params
   * @param {Object} params.object Object to update
   * @param {string} params.tableName Table name as we created in DDBB
   * @param {sequelizeModel} params.model Sequelize model instance
   * @returns {Object} Record updated
   */
  async update ({ object, tableName, model }) {
    try {
      if (!object.id) throw `ID is mandatory in '${tableName}' to update`;

      const row = await model.findByPk(object.id);
      if (!row) throw `ID ${id} in '${tableName}' not found`;

      const rowUpdated = await row.set(object);
      await rowUpdated.save();
      return rowUpdated;
    } catch (error) {
      throw error;
    }
  },
  /**
   * Soft delete (set "deleted_at" to now) or delete (remove the row) depending on the request params "permanent" ('true')
   * @param {object} params
   * @param {number} params.id Record unique ID
   * @param {string} params.tableName Table name as we created in DDBB
   * @param {sequelizeModel} params.model Sequelize model instance
   * @param {boolean} params.hardDelete Delete record permanently
   * @returns {Object} Empty object
   */
  async delete({ id, tableName, model, hardDelete = false }) {
    try {
      const row = await model.findByPk(id);
      if (!row) throw `ID ${id} in '${tableName}' not found`;
      
      await row.destroy({ force: hardDelete });

      return {};
    } catch (error) {
      throw error;
    }
  },
  /**
   * Update "deleted_at" to null for the row with "id" given in request params
   * @param {object} params
   * @param {number} params.id Record unique ID
   * @param {string} params.tableName Table name as we created in DDBB
   * @param {sequelizeModel} params.model Sequelize model instance
   * @returns {Object} Record recovered
   */
  async recover({ id, tableName, model }) {
    try {
      const sql = `
        SELECT *
        FROM ${tableName}
        WHERE
          id = ${id} AND
          deleted_at IS NOT NULL
      `;

      const rows = await db.sequelize.query(sql, { model });
      const row = rows[0];
      if (!row) throw `ID ${id} in '${tableName}' was not soft deleted or not found`;
      
      const rowUpdated = await row.restore();
      return rowUpdated;
    } catch (error) {
      throw error;
    }
  }
}