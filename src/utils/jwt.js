import jwt from 'jsonwebtoken';
import env from '#config/env.js';

export default {
  /**
   * Generate JWT
   * @param {Object} data 
   * @param {number} hours 
   * @returns Token
   */
  create: (data, hours = 24) => {
    return jwt.sign(data, env.SECRET, {
      expiresIn: hours * 3600,
    });
  },
  /**
   * Check if token is valid
   * @param {string} token JWT
   * @returns 
   */
  verify: (token) => {
    return jwt.verify(token, env.SECRET);
  },
};
