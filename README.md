# Tourist destination management portal API

## Description

Simple technical test for Node.js.

### Statement

HotelBediaX, a new client of FDSA and one of the most important fictional companies in the hotel sector, has the need to completely renew its tourist destination management portal.

The objective of this test is that you, a full stack developer, create a web application for HotelBediaX that operates on destinations.

The final application will consist of several modules, the first of which will be the Destinations module, which is the one you are going to develop. This app will allow the navigation between the different modules, although at the moment there is only this one.

The required operations that must be implemented are the following:
- Create a new destination
- Read all created destinations and their data
- Update their data
- Delete them
- Filtering the data

Those are the minimum required operations, but you are free to add others if you feel that they are useful.

Users have created a first sketch of how the screen should be, but you have creative freedom to add any improvement in both usability and look and feel. Every change to the design that you propose will be valued, as long as the minimum needs are covered.

![Front example](front-example.png "Front example")

### Requirements

- Front-end:
  - SPA (Single Page Application)
  - AngularJS
  - It must be prepared to handle a high amount of destinations (more than
200.000 records).
- Back-end:
  - REST API
  - NodeJS
  - You can mock the database
- Navigation
  - Menu listing the distinct modules
- CRUD
  - 4 basic operations (create, read, update, delete)
- Executable, standalone demo web app

## Run with Docker (recommended)

This is the easiest way to run de application. Docker has the correct versions of node (20.13.0) and mysql (8.4.0). You don't need to install them :)

1. Download [Docker Desktop](https://www.docker.com/products/docker-desktop/) and run it
1. Open a terminal and go to the proyect path
1. Execute `npm i`
1. Execute `docker-compose up --build`

At this point you have the API available to test it with postman (**HotelBediaX API.postman_collection.json** in root directory).